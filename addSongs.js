const readline = require("readline-sync");
const fs = require("fs");
const _ = require("lodash");

const ALBUMS = "./src/data/albums.json";
const SONGS = "./src/data/songs.json";
const ARTISTS = "./src/data/artists.json";
const SAI = "./src/data/songArtistInstruments.json";

console.log("Exit with ctrl+c when finished.");
while (true) {
  let albumId = readline.question("Enter album id: ");
  const albumData = fs.readFileSync(ALBUMS, "utf8");
  const albums = JSON.parse(albumData);

  if (!albums.filter(a => a.id === albumId)[0]) {
    console.error("No album found with id " + albumId);
    continue;
  }

  let doneSongs = false;
  while (!doneSongs) {
    const songTitle = readline.question(
      "Enter a song title. Type DONE if there are no more. "
    );

    if (songTitle.toUpperCase() === "DONE") {
      doneSongs = true;
      continue;
    }

    const songId = _.kebabCase(songTitle);
    const songData = fs.readFileSync(SONGS, "utf8");
    const songs = JSON.parse(songData);

    songs.push({ id: songId, title: songTitle });

    albums.forEach(a => {
      if (a.id === albumId) {
        a.songs.push(songId);
      }
    });

    console.log(`Adding ${songTitle} to ${albumId}.`);
    fs.writeFileSync(ALBUMS, JSON.stringify(albums, null, 2), "utf8");
    fs.writeFileSync(SONGS, JSON.stringify(songs, null, 2), "utf8");

    let doneVocalists = false;
    while (!doneVocalists) {
      const vocalistName = readline.question(
        "Enter a vocalist's name. Type DONE if there are no more. "
      );

      if (vocalistName.toUpperCase() === "DONE") {
        doneVocalists = true;
        continue;
      }

      const vocalistId = _.kebabCase(vocalistName);
      const artistsData = fs.readFileSync(ARTISTS, "utf8");
      const artists = JSON.parse(artistsData);

      const artistIds = artists.map(a => a.id);
      if (artistIds.indexOf(vocalistId) < 0) {
        console.log(`${vocalistId} doesn't exist yet. Creating`);
        artists.push({ id: vocalistId, name: vocalistName });
        fs.writeFileSync(ARTISTS, JSON.stringify(artists, null, 2), "utf8");
      }

      const saiData = fs.readFileSync(SAI, "utf8");
      const sais = JSON.parse(saiData);

      console.log(`Adding ${vocalistName} to ${songTitle}`);
      sais.push({
        songId: songId,
        artistId: vocalistId,
        instrumentId: "vocals"
      });

      fs.writeFileSync(SAI, JSON.stringify(sais, null, 2), "utf8");
    }
  }
}
