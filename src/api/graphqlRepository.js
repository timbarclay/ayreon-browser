import ApolloClient from "apollo-boost";
import { gql } from "apollo-boost";
import fetch from "node-fetch";

export default class GraphqlRepository {
  constructor({ host, port, endpoint }) {
    const uri = `http://${host}:${port}/${endpoint}`;
    this.client = new ApolloClient({ uri, fetch });
  }

  getAlbums() {
    const query = gql`
      query allAlbums {
        allAlbums {
          id
          title
          releaseDate
        }
      }
    `;
    return this.client.query({ query }).then(res => res.data.allAlbums);
  }

  getAlbum(id) {
    const query = gql`
      query album($id: String!) {
        album(id: $id) {
          id
          title
          releaseDate
          songs {
            id
            title
          }
        }
      }
    `;
    return this.client
      .query({
        query,
        variables: {
          id
        }
      })
      .then(res => res.data.album);
  }

  getArtists() {
    const query = gql`
      query allArtists {
        allArtists {
          id
          name
        }
      }
    `;
    return this.client.query({ query }).then(res => res.data.allArtists);
  }

  getArtist(id) {
    const query = gql`
      query artist($id: String!) {
        artist(id: $id) {
          id
          name
          songs {
            song {
              id
              title
              album {
                id
                title
              }
            }
            instrument {
              id
              name
            }
          }
        }
      }
    `;
    return this.client
      .query({
        query,
        variables: {
          id
        }
      })
      .then(res => res.data.artist);
  }

  getSongs() {
    const query = gql`
      query allSongs {
        allSongs {
          id
          title
        }
      }
    `;
    return this.client.query({ query }).then(res => res.data.allSongs);
  }

  getSong(id) {
    const query = gql`
      query song($id: String!) {
        song(id: $id) {
          id
          title
          artists {
            artist {
              id
              name
            }
            instrument {
              id
              name
            }
          }
        }
      }
    `;
    return this.client
      .query({
        query,
        variables: {
          id
        }
      })
      .then(res => res.data.song);
  }

  getInstruments() {
    const query = gql`
      query allInstruments {
        allInstruments {
          id
          name
        }
      }
    `;
    return this.client.query({ query }).then(res => res.data.allInstruments);
  }

  getInstrument(id) {
    const query = gql`
      query instrument($id: String!) {
        instrument(id: $id) {
          id
          name
          artists {
            artist {
              id
              name
            }
          }
        }
      }
    `;
    return this.client
      .query({
        query,
        variables: {
          id
        }
      })
      .then(res => res.data.instrument);
  }

  getArtistsOnMostSongs() {
    // TODO
  }

  getArtistsOnMostAlbums() {
    // TODO
  }
}
