import _ from "lodash";
import albums from "../data/albums.json";
import artists from "../data/artists.json";
import songs from "../data/songs.json";
import instruments from "../data/instruments.json";
import songArtistInstruments from "../data/songArtistInstruments.json";

const albumsById = _.keyBy(albums, a => a.id);
const artistsById = _.keyBy(artists, a => a.id);
const songsById = _.keyBy(songs, s => s.id);
const instrumentsById = _.keyBy(instruments, i => i.id);

function albumBySong(songId) {
  return (songsById[songId] || {}).albumId;
}

export default class LocalRepository {
  getAlbums() {
    return Promise.resolve(albums);
  }

  getAlbum(id) {
    const album = albumsById[id];
    if (!album) return Promise.resolve(null);

    const albumSongs = songs.filter(s => s.albumId === id);
    const withResolveSongs = Object.assign({}, album, { songs: albumSongs });

    return Promise.resolve(withResolveSongs);
  }

  getArtists() {
    return Promise.resolve(artists);
  }

  getArtist(id) {
    const artist = artistsById[id];
    if (!artist) return Promise.resolve(null);

    const artistSongs = songArtistInstruments.filter(
      sai => sai.artistId === id
    );

    const resolvedSongs = artistSongs.map(as => {
      const song = songsById[as.songId];
      const instrument = instrumentsById[as.instrumentId];
      const albumId = albumBySong(as.songId);
      const album = albumsById[albumId];
      //return { song, album, instrument };
      return {
        song: {
          ...song,
          album
        },
        instrument
      };
    });

    return Promise.resolve({
      ...artist,
      songs: resolvedSongs
    });
  }

  getSongs() {
    return Promise.resolve(songs);
  }

  getSong(id) {
    const song = songsById[id];
    if (!song) return Promise.resolve(null);

    const artistInstruments = songArtistInstruments.filter(
      sai => sai.songId === id
    );
    const resolvedArtists = artistInstruments.map(ai => {
      const artist = artistsById[ai.artistId];
      const instrument = instrumentsById[ai.instrumentId];
      return { artist, instrument };
    });

    return Promise.resolve({ ...song, artists: resolvedArtists });
  }

  getInstruments() {
    return Promise.resolve(instruments);
  }

  getInstrument(id) {
    const instrument = instrumentsById[id];
    if (!instrument) return Promise.resolve(null);

    const instrumentArtists = songArtistInstruments.filter(
      sai => sai.instrumentId === id
    );
    const uniqueArtistIds = _.uniq(instrumentArtists.map(ia => ia.artistId));
    const resolvedArtists = uniqueArtistIds.map(a => ({
      artist: artistsById[a]
    }));

    return Promise.resolve({ ...instrument, artists: resolvedArtists });
  }

  getArtistsOnMostSongs() {
    const counted = _.countBy(songArtistInstruments, sai => sai.artistId);
    const pairs = _.toPairs(counted);
    const sorted = _.sortBy(pairs, p => 0 - p[1]);
    return Promise.resolve(sorted.map(a => [artistsById[a[0]], a[1]]));
  }

  getArtistsOnMostAlbums() {
    const withAlbum = songArtistInstruments.map(sai => ({
      ...sai,
      albumId: albumBySong(sai.songId)
    }));
    const distinctByAlbum = _.uniqBy(withAlbum, a => a.albumId + a.artistId);
    const counted = _.countBy(distinctByAlbum, sai => sai.artistId);
    const pairs = _.toPairs(counted);
    const sorted = _.sortBy(pairs, p => 0 - p[1]);
    return Promise.resolve(sorted.map(a => [artistsById[a[0]], a[1]]));
  }
}
