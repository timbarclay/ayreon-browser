import config from "../config";

const backend = config.backend;
const repoOptions = config[backend];

// We'll assign the repo here the first time we get it so it's cached and we don't have to find and instantiate it again
let repo;

/**
 * Get the relevant repo based on the backend key set in config
 */
function getRepo() {
  if (repo) {
    return Promise.resolve(repo);
  } else {
    return new Promise((resolve, reject) => {
      chooseRepo(backend)
        .then(backendExport => {
          const backendCtr = backendExport.default;
          resolve(new backendCtr(repoOptions));
        })
        .catch(err => {
          console.error(err);
          reject(err);
        });
    });
  }
}

function chooseRepo(backendName) {
  switch (backendName.toLowerCase()) {
    case "local":
      return import("./localRepository");
    case "graphql":
      return import("./graphqlRepository");
    default:
      throw `${backendName} is not a valid type of backend`;
  }
}

export function getAlbums() {
  return getRepo().then(r => r.getAlbums());
}

export function getAlbum(id) {
  return getRepo().then(r => r.getAlbum(id));
}

export function getArtists() {
  return getRepo().then(r => r.getArtists());
}

export function getArtist(id) {
  return getRepo().then(r => r.getArtist(id));
}

export function getSongs() {
  return getRepo().then(r => r.getSongs());
}

export function getSong(id) {
  return getRepo().then(r => r.getSong(id));
}

export function getInstruments() {
  return getRepo().then(r => r.getInstruments());
}

export function getInstrument(id) {
  return getRepo().then(r => r.getInstrument(id));
}

export function getArtistsOnMostSongs() {
  return getRepo().then(r => r.getArtistsOnMostSongs());
}

export function getArtistsOnMostAlbums() {
  return getRepo().then(r => r.getArtistsOnMostAlbums());
}
