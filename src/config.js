// options: local|graphql
const backend = "graphql";

export default {
  // The version that gets deployed on gitlab pages has to use local as there's no public backend server running
  backend: isCI ? "local" : backend,

  // Below are option objects to be parsed to the constructor of whichever backend repo is specified above. The obj keys must be the same as the options for 'backend'
  local: {},

  graphql: {
    host: "localhost",
    port: "9000",
    endpoint: "graphql"
  }
};
