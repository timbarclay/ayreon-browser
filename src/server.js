import sirv from "sirv";
import polka from "polka";
import compression from "compression";
import * as sapper from "@sapper/server";

const { PORT, NODE_ENV, CI } = process.env;
const dev = NODE_ENV === "development";

// Indicates that we are building in the Gitlab CI environment
const isCiBuild = !!CI;

polka() // You can also use Express
  .use(
    isCiBuild ? "/ayreon-browser" : "/",
    compression({ threshold: 0 }),
    sirv("static", { dev }),
    sapper.middleware()
  )
  .listen(PORT, err => {
    if (err) console.log("error", err);
  });
